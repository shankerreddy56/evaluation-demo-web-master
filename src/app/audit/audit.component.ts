import { Component, OnInit } from "@angular/core";
import { first } from "rxjs/operators";
import { AuditService, AuthenticationService } from "@/_services";
import { IDate } from "./date.interface";

@Component({ templateUrl: "audit.component.html" })
export class AuditComponent implements OnInit {
  audits = [];
  user: any;
  today: number = Date.now();
  selectdate: any = "";
  constructor(
    private authenticationService: AuthenticationService,
    private auditService: AuditService
  ) {}

  ngOnInit() {
    this.loadAllAudits();
  }

  public dates: Array<IDate> = [
    { format: this.today, date: "12hrs" },
    { format: this.today, date: "24hrs" },
  ];
  public dformat: any;
  private loadAllAudits() {
    this.auditService
      .getAll()
      .pipe(first())
      .subscribe((audits) => (this.audits = audits));
  }
  date(): any {
    return "how r u";
  }
  Search() {
    if (this.user == "") {
      this.ngOnInit();
    } else {
      this.audits = this.audits.filter((res) => {
        return res.user
          .toLocaleLowerCase()
          .match(this.user.toLocaleLowerCase());
      });
    }
  }
  key: any = "id";
  reverse: boolean = false;
  p: number = 1;
  sort(key) {
    this.key = key;
    this.reverse = !this.reverse;
  }
}
