﻿export class Audit {
  id: number;
  user: any;
  loginTime: string;
  logoutTime: string;
  ip: string;
}
